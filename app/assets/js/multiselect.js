$(document).ready(function(){

  var methods = {
    init : function() {
      var multiselect=$(this);
      var display=multiselect.find('.display').first();
      var list=multiselect.find('ul').first();
      var items=multiselect.find('li');
      var button=multiselect.find('.caret').first();
      
      items.find('input').change(function(e){
        parent=$(this).closest('li');
        value=$(this).val();
        text=parent.find('span').first().text();
        
        if($(this).is(':checked')){
          methods.add_item(multiselect,value,text);
        }else{
          methods.remove_item(multiselect,value);
        }
      });

      list.hide();
      button.click(function(){
        list.toggle();
      });
    },
    add_item : function(object,value,text) {
      var display=object.find('.display .span_wrapper').first();

      display.prepend('<span data-value="'+value+'">'+text+'</span>');
    },
    remove_item : function(object,value) {
      object.find('.display .span_wrapper span[data-value="'+value+'"]').first().remove();
      
    }

  };

  $.fn.multiselect = function( method ) {

    return this.each(function(){
      // логика вызова метода
      if ( methods[method] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
      }else if( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
      }else{
        $.error( 'Метод с именем ' +  method + ' не существует для jQuery.multiselect' );
      } 
    });

  }
});