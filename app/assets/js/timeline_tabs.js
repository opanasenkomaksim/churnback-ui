$(document).ready(function(){

  var methods = {
    init : function() {
      var tab_btns=$('.tab-btn,input[type="submit"]');

      $('.tab').each(function(){
        if($(this).hasClass('active')){
          $(this).show();
        }else{
          $(this).hide();
        }
      });

      tab_btns.click(function(){
        if($('.tab'+$(this).attr('href')).length==1||$(this).is($('input[type="submit"]'))){
          if(!$(this).is($('input[type="submit"]'))){
            tab=$('.tab'+$(this).attr('href'));
          }else{
            tab=$('.tab.active');
          }
          parent=tab.closest('.tabs');
          active_tab=parent.find('.tab.active');
          tabs=parent.find('.tab');

          active_tab_index=tabs.index(active_tab);
          if(tabs.index(tab)>=active_tab_index){
            switch(active_tab_index) {
                case 0:
                    if(!methods.validate_1_tab())return false;
                    break;
                case 1:
                    if(!methods.validate_1_tab()||!methods.validate_2_tab())return false;
                    break;
                case 2:
                    if(!methods.validate_1_tab()||!methods.validate_2_tab()||!methods.validate_3_tab())return false;
                    break;
                case 3:
                    if(!methods.validate_1_tab()||!methods.validate_2_tab()||!methods.validate_3_tab()||!methods.validate_4_tab())return false;
                    break;
                case 4:
                    if(!methods.validate_1_tab()||!methods.validate_2_tab()||!methods.validate_3_tab()||!methods.validate_4_tab()||!methods.validate_5_tab())return false;
                    break;
            }
          }

          active_tab.removeClass('active');
          tab.addClass('active');

          tabs.each(function(){
            if($(this).hasClass('active')){
              $(this).show();
            }else{
              $(this).hide();
            }
          });

          active_tab=parent.find('.tab.active');
          active_tab_index=tabs.index(active_tab);

          var arr=[0,1,2,2,3];
          // alert(arr[active_tab_index]);

          cols=$('.tabs-nav .col');

          cols.each(function(){
            if(cols.index($(this))+1<arr[active_tab_index]){
              $(this).addClass('filled');
              $(this).removeClass('active');
            }else if(cols.index($(this))+1==arr[active_tab_index]){
              $(this).addClass('filled');
              $(this).addClass('active');
            }else{
              $(this).removeClass('filled');
              $(this).removeClass('active');
            }
          });

          if(!$(this).is($('input[type="submit"]')))
            return false;
        }
      });

      $('input#plan').click(function(){
        var parent=$(this).closest('.row.plan');
        var plan_name=parent.find('.plan_name').first();
        var plan_price=parent.find('.plan_price').first();

        $('#tab-4 .bg .selected-plan').remove();
        $('#tab-4 .bg').prepend(
        '<div class="row selected-plan">'+
          '<div class="col">'+
            '<div class="title">'+plan_name.html()+'</div>'+
            '<div class="subtitle">750 prospects/month</div>'+
            '<div class="voffset-10"></div>'+
          '</div>'+
          '<div class="col-auto text-right">'+
            '<div class="title-price">'+plan_price.html()+'</div>'+
          '</div>'+
        '</div>'
        );
      });


      $('input[type="text"],input[type="email"],input[type="password"]').wrap(function(){
        return '<div class="input_container '+$(this).attr('type')+'"></div>';
      });
      $('.plans,.plans-footer').wrap('<div class="input_container radio"></div>');

      $('input[type!="submit"]').focus(function() {
        $(this).closest('.tab').find('.validation_msg').remove();
        $(this).closest('.tab').find('.input_container').removeClass('invalid')
      });

      // tab-1 fields validation
      $('input#email').change(function() {
        test = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#company_name').change(function() {
        test = ($(this).val().length<2||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#website_url').change(function() {
        test = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      // tab-2 fields validation
      $('input#first_name').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#last_name').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#password').change(function() {
        test = ($(this).val().length<6||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#company_name_2').change(function() {
        test = ($(this).val().length<2||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#address').change(function() {
        test = ($(this).val().length<5||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#city').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#country').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      // ^(
      // (AT)?U[0-9]{8} |                              # Austria
      // (BE)?0[0-9]{9} |                              # Belgium
      // (BG)?[0-9]{9,10} |                            # Bulgaria
      // (CY)?[0-9]{8}L |                              # Cyprus
      // (CZ)?[0-9]{8,10} |                            # Czech Republic
      // (DE)?[0-9]{9} |                               # Germany
      // (DK)?[0-9]{8} |                               # Denmark
      // (EE)?[0-9]{9} |                               # Estonia
      // (EL|GR)?[0-9]{9} |                            # Greece
      // (ES)?[0-9A-Z][0-9]{7}[0-9A-Z] |               # Spain
      // (FI)?[0-9]{8} |                               # Finland
      // (FR)?[0-9A-Z]{2}[0-9]{9} |                    # France
      // (GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3}) | # United Kingdom
      // (HU)?[0-9]{8} |                               # Hungary
      // (IE)?[0-9]S[0-9]{5}L |                        # Ireland
      // (IT)?[0-9]{11} |                              # Italy
      // (LT)?([0-9]{9}|[0-9]{12}) |                   # Lithuania
      // (LU)?[0-9]{8} |                               # Luxembourg
      // (LV)?[0-9]{11} |                              # Latvia
      // (MT)?[0-9]{8} |                               # Malta
      // (NL)?[0-9]{9}B[0-9]{2} |                      # Netherlands
      // (PL)?[0-9]{10} |                              # Poland
      // (PT)?[0-9]{9} |                               # Portugal
      // (RO)?[0-9]{2,10} |                            # Romania
      // (SE)?[0-9]{12} |                              # Sweden
      // (SI)?[0-9]{8} |                               # Slovenia
      // (SK)?[0-9]{10}                                # Slovakia
      // )$
      $('input#vat_number').change(function() {
        test = /^((AT)?U[0-9]{8}|(BE)?0[0-9]{9}|(BG)?[0-9]{9,10}|(CY)?[0-9]{8}L|(CZ)?[0-9]{8,10}|(DE)?[0-9]{9}|(DK)?[0-9]{8}|(EE)?[0-9]{9}|(EL|GR)?[0-9]{9}|(ES)?[0-9A-Z][0-9]{7}[0-9A-Z]|(FI)?[0-9]{8}|(FR)?[0-9A-Z]{2}[0-9]{9}|(GB)?([0-9]{9}([0-9]{3})?|[A-Z]{2}[0-9]{3})|(HU)?[0-9]{8}|(IE)?[0-9]S[0-9]{5}L|(IT)?[0-9]{11}|(LT)?([0-9]{9}|[0-9]{12})|(LU)?[0-9]{8}|(LV)?[0-9]{11}|(MT)?[0-9]{8}|(NL)?[0-9]{9}B[0-9]{2}|(PL)?[0-9]{10}|(PT)?[0-9]{9}|(RO)?[0-9]{2,10}|(SE)?[0-9]{12}|(SI)?[0-9]{8}|(SK)?[0-9]{10})$/.test( $(this).val() );
        methods.validate($(this),test);
      });

      // tab-3 fields validation
      $('input[name="plan"]').change(function() {
        test=($('input[name="plan"]:checked').length<1)?false:true;
        methods.validate($(this),test);
      });

      $('input[name="billing_cycle"]').change(function() {
        test=($('input[name="billing_cycle"]:checked').length<1)?false:true;
        methods.validate($(this),test);
      });

      // tab-4 fields validation
      // ^(?:4[0-9]{12}(?:[0-9]{3})? # Visa
      //  |  (?:5[1-5][0-9]{2}                # MasterCard
      //      | 222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}
      //  |  3[47][0-9]{13}                   # American Express
      //  |  3(?:0[0-5]|[68][0-9])[0-9]{11}   # Diners Club
      //  |  6(?:011|5[0-9]{2})[0-9]{12}      # Discover
      //  |  (?:2131|1800|35\d{3})\d{11}      # JCB
      // )$
      $('input#credit-card').change(function() {
        test = /^(?:4[0-9]{12}(?:[0-9]{3})?|(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}|3[47][0-9]{13})$/.test( $(this).val() )
        methods.validate($(this),test);
      });

      $('input#cvc').change(function() {
        test = /^[0-9]{3,4}$/.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#full-name').change(function() {
        test = /^[a-z\.]*\s?([a-z\-\']+\s)+[a-z\-\']+$/.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#mm').change(function() {
        test = /^((0[1-9])|(1[0-2]))$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#dd').change(function() {
        test = /^(\d{2})$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#company').change(function() {
        test = ($(this).val().length<2||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#address_2').change(function() {
        test = ($(this).val().length<5||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#postal').change(function() {
        test = /^[0-9]{5,6}$/.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#city_2').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#country_2').change(function() {
        test = /^[a-z ,.'-]{2,20}$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      // tab-5 fields validation
      $('input#stripe_webhook').change(function() {
        test = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

      $('input#retry_every').change(function() {
        test = ($(this).val().length<5||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#retry_period').change(function() {
        test = ($(this).val().length<5||$(this).val().length>30)?false:true;
        methods.validate($(this),test);
      });

      $('input#update_page').change(function() {
        test = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( $(this).val() );
        methods.validate($(this),test);
      });

    },
    validate: function(element,valid,msg="Wrong field value!") {
      $(this).closest('.tab').find('.validation_msg').remove();

      if(element.attr('type')=='radio'){
        msg="All fields are mandatory please fill them up to finish your registration";
        input_container=element.closest('.input_container');
        if(!valid){
          input_container.removeClass('valid');
          input_container.addClass('invalid');
          input_container.append('<div class="validation_msg">'+msg+'</div>');
        }else{
          input_container.addClass('valid');
          input_container.removeClass('invalid');
          input_container.find('.validation_msg').remove();
        }
      }else if(element.attr('id')=="mm"||element.attr('id')=="dd"){
        if(element.val().length<1)msg="All fields are mandatory please fill them up to finish your registration";
        input_container=element.closest('.input_container');
        if(element.closest('.input_container').not('.row').length>0){
          element.unwrap();
          input_container=element.closest('.row').addClass('input_container');
        }
        if(!valid){
          input_container.removeClass('valid');
          input_container.addClass('invalid');
          input_container.append('<div class="validation_msg">'+msg+'</div>');
        }else{
          input_container.addClass('valid');
          input_container.removeClass('invalid');
          input_container.find('.validation_msg').remove();
        }
      }else{
        if(element.val().length<1)msg="All fields are mandatory please fill them up to finish your registration";
        input_container=element.closest('.input_container');
        if(!valid){
          input_container.removeClass('valid');
          input_container.addClass('invalid');
          input_container.append('<div class="validation_msg">'+msg+'</div>');
        }else{
          input_container.addClass('valid');
          input_container.removeClass('invalid');
          input_container.find('.validation_msg').remove();
        }
      }
    },
    validate_1_tab : function() {
      // $('#tab-1 input[type!="submit"]').change();

      var valid = true;
      // $('#tab-1 .input_container').each(function(){
      //   if(!$(this).hasClass('valid')){
      //     valid=false;
      //     return false;
      //   }
      // });

      // if(!valid)alert('Tab 1 fields filled wrong!');
      return valid;
    },
    validate_2_tab : function() {
      // $('#tab-2 input[type!="submit"]').change();

      var valid = true;
      // $('#tab-2 .input_container').each(function(){
      //   if(!$(this).hasClass('valid')){
      //     valid=false;
      //     return false;
      //   }
      // });

      // if(!valid)alert('Tab 2 fields filled wrong!');
      return valid;
    },
    validate_3_tab : function() {
      // $('#tab-3 input[type!="submit"]').change();

      var valid = true;
      // $('#tab-3 .input_container').each(function(){
      //   if(!$(this).hasClass('valid')){
      //     valid=false;
      //     return false;
      //   }
      // });

      // if(!valid)alert('Tab 3 fields filled wrong!');
      return valid;
    },
    validate_4_tab : function() {
      // $('#tab-4 input[type!="submit"]').change();

      var valid = true;
      // $('#tab-4 .input_container').each(function(){
      //   if(!$(this).hasClass('valid')){
      //     valid=false;
      //     return false;
      //   }
      // });

      // if(!valid)alert('Tab 4 fields filled wrong!');
      return valid;
    },
    validate_5_tab : function() {
      $('#tab-5 input[type!="submit"]').change();

      var valid = true;
      $('#tab-5 .input_container').each(function(){
        if(!$(this).hasClass('valid')){
          valid=false;
          return false;
        }
      });

      // if(!valid)alert('Tab 5 fields filled wrong!');
      return valid;
    }

  };

  $.fn.timeline_tabs = function( method ) {

    return this.each(function(){
      // логика вызова метода
      if ( methods[method] ) {
        return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
      }else if( typeof method === 'object' || ! method ) {
        return methods.init.apply( this, arguments );
      }else{
        $.error( 'Метод с именем ' +  method + ' не существует для jQuery.timeline_tabs' );
      } 
    });

  }
});